# Metadata Service

The metadata service is in charge of getting extra patient information that doesn't really belong in the Patient Data Service. Capable of doing the same as PDS, but with different data.

Dependencies:
- PHP 8.1
- Laravel 10
- PHP Extensions
