-- extra information about patient.
CREATE TABLE metadata (
    patient_id INT PRIMARY KEY,
    insurance_provider VARCHAR(100),
    policy_number VARCHAR(50),
    emergency_contact_name VARCHAR(100),
    emergency_contact_relationship VARCHAR(50),
    emergency_contact_number VARCHAR(15),
    allergen_name VARCHAR(100),
    allergy_severity VARCHAR(50),
    allergy_reaction_details TEXT,
    medical_alerts TEXT,
    preferred_language VARCHAR(50),
    accessibility_needs TEXT,
    financial_information TEXT,
    research_study_participation TEXT,
    referring_physician VARCHAR(100),
    referral_date DATE,
    consent_records TEXT
);

-- for the scope of this project, we will store only patient x rays. other tables can be added for images however.
CREATE TABLE patient_extra_images (
    image_id INT PRIMARY KEY,
    patient_id INT,
    image_path VARCHAR(255),
    FOREIGN KEY (patient_id) REFERENCES Metadata(patient_id)
);
