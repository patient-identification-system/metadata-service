<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('metadata', function (Blueprint $table) {
            //
            $table->string('medverify_id', 50)->default(Str::uuid());
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('metadata', function (Blueprint $table) {
            //
            $table->dropColumn('medverify_id');
        });
    }
};
