<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * CREATE TABLE metadata (
         * patient_id INT PRIMARY KEY,
         * insurance_provider VARCHAR(100),
         * policy_number VARCHAR(50),
         * emergency_contact_name VARCHAR(100),
         * emergency_contact_relationship VARCHAR(50),
         * emergency_contact_number VARCHAR(15),
         * allergen_name VARCHAR(100),
         * allergy_severity VARCHAR(50),
         * allergy_reaction_details TEXT,
         * medical_alerts TEXT,
         * preferred_language VARCHAR(50),
         * accessibility_needs TEXT,
         * financial_information TEXT,
         * research_study_participation TEXT,
         * referring_physician VARCHAR(100),
         * referral_date DATE,
         * consent_records TEXT
     * );
     */
    public function up(): void
    {
        Schema::create('metadata', function (Blueprint $table) {
            //
            $table->bigIncrements('metadata_id');
            $table->unsignedBigInteger('patient_id');
            $table->string('insurance_provider', 100);
            $table->string('policy_number', 50);
            $table->string('emergency_contact_name', 100);
            $table->string('emergency_contact_relationship', 50);
            $table->string('emergency_contact_number', 15);
            $table->string('allergen_name', 100);
            $table->string('allergy_severity', 50);
            $table->text('allergy_reaction_details');
            $table->text('medical_alerts');
            $table->string('preferred_language', 50);
            $table->text('accessibility_needs');
            $table->text('financial_information');
            $table->text('research_study_participation');
            $table->string('referring_physician', 100);
            $table->date('referral_date');
            $table->text('consent_records');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('metadata');
    }
};
