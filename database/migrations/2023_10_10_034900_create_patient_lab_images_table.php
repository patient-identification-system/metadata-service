<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('patient_lab_images', function (Blueprint $table) {
            //
            $table->bigIncrements('image_id');
            $table->unsignedBigInteger('metadata_id');
            $table->string('image_path', 255);
            $table->timestamps();

            $table->foreign('metadata_id')->references('metadata_id')->on('metadata')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('patient_lab_images');
    }
};
