<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Metadata extends Model
{
    use HasFactory;

    protected $primaryKey = 'metadata_id';

    public function patientLabImages(): HasMany
    {
        return $this->hasMany(PatientLabImage::class, 'image_id');
    }
}
