<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\MetadataCreateRequest;
use App\Http\Requests\MetadataUpdateRequest;
use App\Models\Metadata;

class MetadataController extends Controller
{
    //
    public function show(string $patientId)
    {
        $statusCode = 404;
        //
        $metadata = Metadata::where('medverify_id', $patientId)->with('patientLabImages')->get();

        // set to 200 if results are found.
        if ($metadata->count() != 0) {
            $statusCode = 200;
        }

        return response()->json(['data' => $metadata], $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MetadataCreateRequest $request)
    {
        $statusCode = 400;

        // Creates a new patient record and loads it into the database.
        $metadata = Metadata::create($request->toArray());

        if ($metadata->save()) {
            // status code 201 for the resource that was created
            $statusCode = 201;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MetadataUpdateRequest $request, string $id)
    {
        // default status code
        $statusCode = 304;

        $metadata = Metadata::where('patient_id', $id)
            ->update($request->toArray());

        if ($metadata) {
            $statusCode = 204;
        }

        return response()->json([], $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $metadata = Metadata::where('patient_id', $id)->first();

        if($metadata) {
            $metadata->delete();

            // return resource deleted status
            return response()->json(
                [], 204
            );
        }

        return response()->json([], 410);
    }
}
